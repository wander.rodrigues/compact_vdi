
import os
import sys
import subprocess as shell

# Seleciona modo de execução ou simulação
try:
    execute_mode = (sys.argv[1] == '--execute')
except:
    execute_mode = False

if(not(execute_mode)):
    print('\n', 31 * '*', '\n Executando em modo de simulação\n', 31 * '*')

# Monta o caminho até VirtualBox VMs
homedir = os.environ.get('HOME')
vm_path = os.path.join(homedir, 'VirtualBox VMs')

# Cria um dicionário com Caminho : Lista_vdi para todas as VMs
dict_vdi = {}
for dirpath, dirnames, filenames in os.walk(vm_path):
    vdi_files = [f for f in filenames if(f[-4:] == '.vdi')]
    if(vdi_files):
        dict_vdi[dirpath] = vdi_files

t = sum([len(dict_vdi[i]) for i in dict_vdi])
n = 0
# Cria uma lista com o caminho completo de cada .vdi
lista = []
for p in dict_vdi:
    os.chdir(p)
    print('\n', p)
    for l in dict_vdi[p]:
        l = l.replace('{', '\{')
        l = l.replace('}', '\}')
        lista.append(os.path.join(p, l))
        cmd = 'vboxmanage modifyhd ' + l + ' compact'  # Monta comando para ser executado no Shell
        # print(cmd)
        n += 1
        print('{:d} de {:d}'.format(n, t))
        try:
            if(execute_mode):
                shell.run(cmd, shell=True, check=True)  # Executa comando no Shell
                print('\tSucesso em: ', cmd)
            else:
                print(l)
        except Exception as e:
            # e = sys.exc_info()[0]
            print("\tO seguinte erro aconteceu: ", e)


# for linha in lista:
#    print(linha)
